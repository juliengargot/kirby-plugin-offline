<?php
/**
 * Offline Plugin
 *
 * Simply allow to put kirby in “offline” mode by publishing or not homepage.
 * c::set('offline.default.title','Site offline'); // default option
 * c::set('offline.default.message','We will be back shortly.'); // default option
 * c::set('environment','local);
 *
 * @author Julien Gargot <julien@g-u-i.me>
 * @version 1.0.2
 */

$kirby->set('blueprint', 'fields/offlinetitle', __DIR__ . '/blueprints/fields/offlinetitle.yml');
$kirby->set('blueprint', 'fields/offlinemessage', __DIR__ . '/blueprints/fields/offlinemessage.yml');
$kirby->set('template', 'offline', __DIR__ . '/templates/offline.php');

$site     = $kirby->site();
$homepage = $site->homepage();
$path     = $kirby->request()->path();

if (
  $homepage->isInvisible()
  && !$site->user()
  && c::get('environment') != 'local'
  &&
  (
    $path->count()
    && $path->nth(0) != 'panel'
    && $path->nth(0) != 'login'
    ||
    (
      $path->count() == 0
      && rtrim($kirby->request()->url(),"/") == $kirby->site()->url()
    )
  )
)
{
  echo tpl::load(__DIR__ . '/templates/offline.php', $homepage, false);
}
