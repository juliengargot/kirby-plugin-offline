# Kirby *Offline* plugin

In the context of Kirby, the plugin simply allows you to put the site offline for non logged users by setting home page to “invisible”.
Look and template of the message is directly taken from the default Kirby error page.

## Requirement and Settings

None. Ready to go when installed.

## Options

You can set the default title and message that will be display to non logged users from your the [site config](https://getkirby.com/docs/developer-guide/configuration/options) using

```
c::set('offline.default.title','Site offline');
```
and
```
c::set('offline.default.message','We will be back shortly.');
```

If you want let administrators use their own values, add fields `offlinetitle: offlinetitle` and `offlinemessage: offlinemessage` to the *site* blueprint.

You can also prevent offline mode in local environment using `c::set('environment','local)` in your local config file. Cf. [Multi-environment setup](https://getkirby.com/docs/developer-guide/configuration/options).

## Installation

Download or clone the repository in `/site/plugins`.

### TODO:

- [x] ~~Extend plugin to redirect all invisible pages to the error page (or custom) for non logged user~~. I did it in another plugin [here](https://github.com/julien-gargot/kirby-plugin-invisible)
- [ ] Find a visual hint for logged-in user so they know the site is offline.
- [ ] Find a visual hint for logged-in user so they know the page invisible.
- [ ] Add the possibility to use a site field to put site offline instead of using homepage visibility ([discussion here](https://forum.getkirby.com/t/maintenance-mode-differentiation-visitor-admin/9141/7?u=julieng)).
