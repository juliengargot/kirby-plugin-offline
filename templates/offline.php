<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Offline</title>

  <style>
    body {
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
      padding: 10%;
      line-height: 1.5em;
    }
    a {
      color: inherit;
    }
    a:hover,
    a:focus {
      color: #000;
    }
    .text {
      text-align: center;
    }
    pre{
      line-height: 1.2;
    }
    p {
      max-width: 30em;
      margin: 0 auto;
    }
    .notice {
      font-weight: bold;
    }
    p:not(.notice) {
      font-size: .8em;
      font-style: italic;
      color: #999;
      padding-top: 3rem;
    }
  </style>

</head>
<body>
  <div class="text">
    <p class="notice">
      <?php echo $site->offlinetitle()->isNotEmpty() ? $site->offlinetitle()->escape() : c::get('offline.default.title',"Site offline")?>
    </p>
    <?php echo $site->offlinemessage()->isNotEmpty() ? $site->offlinemessage()->kirbytext() : "<p>". c::get('offline.default.message',"We will be back shortly.") ."</p>"?>
  </div>
</body>
</html>
<?php exit; ?>
